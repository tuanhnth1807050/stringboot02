package com.example.springboot02.controller;


import com.example.springboot02.entity.User;
import com.example.springboot02.model.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@Controller
public class UserController {

    //DI call model
    @Autowired
    UserRepository userRepository;
    //step1
    @RequestMapping("/")
    public  String index(Model model)
    {
        //Call biz -> Step2
        List<User> users = (List<User>) userRepository.findAll();
        //Step3
        //request.setAttribute("user",users); or session.setAttribute("user",users);
        model.addAttribute("users",users);
        return "index";
    }
    @RequestMapping(value = "add")
    public String addUser(Model model)
    {
        model.addAttribute("user",new User());
        return "addUser";

    }
    //Step1
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String save(User user)
    {
        //Step 2
        userRepository.save(user);
        return "redirect:/";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String editUser(@RequestParam("id") Long userId, Model model)
    {
        Optional<User> usersEdit = userRepository.findById(userId);
        usersEdit.ifPresent(user -> model.addAttribute("user", user));
        return "editUser";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String deleteUser(@RequestParam("id") Long userId, Model model)
    {
        userRepository.deleteById(userId);
        return "redirect:/";
    }


}

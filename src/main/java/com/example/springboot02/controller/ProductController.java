package com.example.springgboot02.controller;

import com.example.springgboot02.entity.Product;
import com.example.springgboot02.entity.User;
import com.example.springgboot02.model.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.awt.*;
import java.util.List;
import java.util.Optional;

@Controller
public class ProductController {

    @Autowired
    ProductRepository productRepository;

    @RequestMapping("/product")
    public String index(Model model)
    {
        List<Product> products = (List<Product>) productRepository.findAll();
        model.addAttribute("products",products);

        return "TemplateProduct/index";
    }
    @RequestMapping(value = "addProduct")
    public String addProduct(Model model)
    {
        model.addAttribute("products",new Product());
        return "TemplateProduct/addProducts";

    }

    @RequestMapping(value = "/saveProduct", method = RequestMethod.POST)
    public String saveProduct(Product product)
    {
        productRepository.save(product);
        return "redirect:/";

    }


    @RequestMapping(value = "/editProduct", method = RequestMethod.GET)
    public String editProduct(@RequestParam("id") Integer productId,Model model)
    {
        Optional<Product>  productEdit = productRepository.findById(productId);

        productEdit.ifPresent(product -> model.addAttribute("product",product));
        return "TemplateProduct/editProduct";
    }
    @RequestMapping(value = "/deleteProduct", method = RequestMethod.GET)
    public String deleteProduct(@RequestParam("id") Integer productId, Model model)
    {
        productRepository.deleteById(productId);
        return "redirect:/";
    }



}
